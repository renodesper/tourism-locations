# Tourism and Co.

A new Flutter project which list a lot of tourism locations in Japan.

## Development

```sh
flutter run
```

## Prerequisites

> This is needed if you want to build the project into APK

Make sure `key.properties` exists in `./android/` and `storeFile` inside it refer to the right `.jks` file. Do not forget to also set `keyAlias`, `keyPassword`, and `storePassword` in `key.properties` based on the `.jks` file.

## Build APK

```sh
flutter build apk
```

## Install APK to android device

```sh
flutter Install
```
