import 'package:tourism_n_co/models/location.dart';
import 'package:test_api/test_api.dart';

void main() {
  test('location deserialization', () async {
    final locations = await Location.fetchAll();

    for (var location in locations) {
      expect(location.name, hasLength(greaterThan(0)));
      expect(location.url, hasLength(greaterThan(0)));

      final fetchedLocation = await Location.fetchByID(location.id);
      expect(fetchedLocation.name, equals(location.name));
      expect(fetchedLocation.url, equals(location.url));
      expect(fetchedLocation.facts, hasLength(greaterThan(0)));
    }
  });
}
