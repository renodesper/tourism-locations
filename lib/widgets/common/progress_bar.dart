import 'package:flutter/material.dart';

Widget progressBar(BuildContext ctx) {
  return LinearProgressIndicator(
    value: null,
    backgroundColor: Colors.white,
    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue[600]),
  );
}
