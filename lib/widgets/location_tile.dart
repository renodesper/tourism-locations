import 'package:flutter/material.dart';
import 'package:tourism_n_co/models/location.dart';
import 'package:tourism_n_co/styles/location_tile.dart';

class LocationTile extends StatelessWidget {
  final Location location;
  final bool darkTheme;

  LocationTile({this.location, this.darkTheme});

  @override
  Widget build(BuildContext ctx) {
    final title = this.location.name.toUpperCase();
    final subtitle = this.location.userItinerarySummary.toUpperCase();
    final caption = this.location.tourPackageName.toUpperCase();
    return Container(
      height: LocationTileStyles.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '$title',
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: this.darkTheme
                ? LocationTileStyles.titleDark
                : LocationTileStyles.titleLight,
          ),
          Text('$subtitle', style: LocationTileStyles.subtitle),
          Text('$caption', style: LocationTileStyles.caption),
        ],
      ),
    );
  }
}
