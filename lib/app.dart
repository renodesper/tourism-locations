import 'package:tourism_n_co/pages/location_list.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return MaterialApp(home: LocationList());
  }
}
