import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tourism_n_co/models/location.dart';
import 'package:tourism_n_co/pages/location_detail.dart';
import 'package:tourism_n_co/styles/location_list.dart';
import 'package:tourism_n_co/widgets/common/app_bar.dart';
import 'package:tourism_n_co/widgets/common/progress_bar.dart';
import 'package:tourism_n_co/widgets/banner_image.dart';
import 'package:tourism_n_co/widgets/location_tile.dart';

class LocationList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LocationListState();
}

class _LocationListState extends State<LocationList> {
  bool showProgressBar = false;
  List<Location> locations = [];

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: RefreshIndicator(
        onRefresh: _loadData,
        child: Column(
          children: <Widget>[
            this.showProgressBar
                ? progressBar(ctx)
                : Expanded(
                    child: _renderBody(ctx),
                  ),
          ],
        ),
      ),
    );
  }

  Future<void> _loadData() async {
    setState(() => this.showProgressBar = true);

    final locations = await Location.fetchAll();

    if (mounted) {
      setState(() {
        this.locations = locations;
        this.showProgressBar = false;
      });
    }
  }

  Widget _renderBody(BuildContext ctx) {
    return ListView.builder(
      itemCount: this.locations.length,
      itemBuilder: _listViewItemBuilder,
    );
  }

  Widget _listViewItemBuilder(BuildContext ctx, int index) {
    final location = this.locations[index];

    return GestureDetector(
      onTap: () => _navigateToLocationDetail(ctx, location.id),
      child: Container(
        height: LocationListStyles.itemHeight,
        child: Stack(children: [
          BannerImage(
            url: location.url,
            height: LocationListStyles.itemHeight,
          ),
          _tileFooter(location),
        ]),
      ),
    );
  }

  Widget _tileFooter(Location location) {
    final info = LocationTile(location: location, darkTheme: true);
    final overlay = Container(
      padding: EdgeInsets.symmetric(
        vertical: LocationListStyles.overlayVerticalPadding,
        horizontal: LocationListStyles.overlayHorizontalPadding,
      ),
      decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
      child: info,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [overlay],
    );
  }

  void _navigateToLocationDetail(BuildContext ctx, int locationId) {
    Navigator.push(
        ctx,
        MaterialPageRoute(
          builder: (ctx) => LocationDetail(locationId),
        ));
  }
}
