import 'dart:async';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:tourism_n_co/constants/location_detail.dart';
import 'package:tourism_n_co/models/location.dart';
import 'package:tourism_n_co/styles/common.dart';
import 'package:tourism_n_co/styles/location_detail.dart';
import 'package:tourism_n_co/widgets/common/app_bar.dart';
import 'package:tourism_n_co/widgets/common/progress_bar.dart';
import 'package:tourism_n_co/widgets/banner_image.dart';
import 'package:tourism_n_co/widgets/location_tile.dart';

class LocationDetail extends StatefulWidget {
  final int locationId;

  LocationDetail(this.locationId);

  @override
  State<StatefulWidget> createState() => _LocationDetailState(this.locationId);
}

class _LocationDetailState extends State<LocationDetail> {
  final int locationId;
  bool showProgressBar = false;
  Location location = Location.initialize();

  _LocationDetailState(this.locationId);

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: RefreshIndicator(
        onRefresh: _loadData,
        child: Column(
          children: <Widget>[
            this.showProgressBar
                ? progressBar(ctx)
                : Expanded(
                    child: Stack(
                      children: <Widget>[
                        _renderBody(ctx),
                        _renderFooter(ctx),
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  Future<void> _loadData() async {
    setState(() => this.showProgressBar = true);

    final location = await Location.fetchByID(this.locationId);

    if (mounted) {
      setState(() {
        this.location = location;
        this.showProgressBar = false;
      });
    }
  }

  Widget _renderBody(BuildContext ctx) {
    var contents = List<Widget>();

    contents.add(BannerImage(
      url: this.location.url,
      height: LocationDetailStyles.bannerImageHeight,
    ));
    contents.add(_renderContentHeader(ctx));
    contents.addAll(_renderFacts(ctx, this.location));
    contents.add(_renderFooterSpace());

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: contents,
      ),
    );
  }

  Widget _renderContentHeader(BuildContext ctx) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: LocationDetailStyles.headerVerticalPadding,
        horizontal: LocationDetailStyles.headerHorizontalPadding,
      ),
      child: LocationTile(location: this.location, darkTheme: false),
    );
  }

  List<Widget> _renderFacts(BuildContext ctx, Location location) {
    var facts = List<Widget>();

    for (int i = 0; i < location.facts.length; i++) {
      facts.add(_factTitle(location.facts[i].title));
      facts.add(_factDescription(location.facts[i].text));
    }

    return facts;
  }

  Widget _factTitle(String title) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: LocationDetailStyles.factVerticalPadding,
        horizontal: LocationDetailStyles.factHorizontalPadding,
      ),
      child: Text(
        title.toUpperCase(),
        textAlign: TextAlign.left,
        style: LocationDetailStyles.headerLarge,
      ),
    );
  }

  Widget _factDescription(String text) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: LocationDetailStyles.factVerticalPadding,
        horizontal: LocationDetailStyles.factHorizontalPadding,
      ),
      child: Text(text, style: CommonStyles.textDefault),
    );
  }

  _renderFooter(BuildContext ctx) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(color: Colors.white.withOpacity(0.5)),
          height: LocationDetailStyles.footerHeight,
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: 20.0,
              horizontal: 30.0,
            ),
            child: _renderBookButton(),
          ),
        )
      ],
    );
  }

  Widget _renderBookButton() {
    return FlatButton(
      color: CommonStyles.accentColor,
      textColor: CommonStyles.textColorBright,
      onPressed: _handleBookPress,
      child: Text(
        'Book'.toUpperCase(),
        style: LocationDetailStyles.textCTAButton,
      ),
    );
  }

  void _handleBookPress() async {
    const url = LocationDetailConstants.mailUrl;

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget _renderFooterSpace() {
    return Container(height: LocationDetailStyles.footerHeight + 10.0);
  }
}
