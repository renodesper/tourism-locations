import 'package:tourism_n_co/models/location.dart';
import 'package:tourism_n_co/models/location_fact.dart';

class MockLocation extends Location {
  static final List<Location> items = [
    Location(
      name: 'Arashiyama Bamboo Grove',
      url: 'https://fluttercrashcourse.com/assets/images/arashiyama@3x.jpg',
      facts: <LocationFact>[
        LocationFact(
            title: 'Summary',
            text:
                'While we could go on about the ethereal glow and seemingly endless heights of this bamboo grove on the outskirts of Kyoto, the sight\'s pleasures extend beyond the visual realm'),
        LocationFact(
            title: 'How to Get There',
            text:
                'Kyoto airport, with several terminals, is located 16 kilometres south of the city and is also known as Kyoto. Kyoto can also be reached by transport links from other regional airports.')
      ],
    ),
    Location(
      name: 'Mount Fuji',
      url:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Mount_Fuji_Japan_with_Snow%2C_Lakes_and_Surrounding_Mountains.jpg/800px-Mount_Fuji_Japan_with_Snow%2C_Lakes_and_Surrounding_Mountains.jpg',
      facts: <LocationFact>[
        LocationFact(
            title: 'Summary',
            text:
                'Mount Fuji, located on Honshū, is the highest mountain in Japan at 3,776.24 m (12,389 ft), 2nd-highest peak of an island (volcanic) in Asia, and 7th-highest peak of an island in the world. It is a dormant stratovolcano that last erupted in 1707–1708.'),
        LocationFact(
            title: 'How to Get There',
            text:
                'The closest airport with scheduled international service is Mt. Fuji Shizuoka Airport. It opened in June 2009. It is about 80 kilometres (50 mi) from Mount Fuji. The major international airports serving Tokyo, Tokyo International Airport (Haneda Airport) in Tokyo and Narita International Airport in Chiba, are hours from Mount Fuji.')
      ],
    ),
    Location(
      name: 'Odaiba',
      url: 'https://www.japan-guide.com/g18/740/3008_01.jpg',
      facts: <LocationFact>[
        LocationFact(
            title: 'Summary',
            text:
                'Odaiba (お台場) is a popular shopping and entertainment district on a man made island in Tokyo Bay. It originated as a set of small man made fort islands (daiba literally means "fort"), which were built towards the end of the Edo Period (1603-1868) to protect Tokyo against possible attacks from the sea and specifically in response to the gunboat diplomacy of Commodore Perry.'),
        LocationFact(
            title: 'How to Get There',
            text:
                'Access to Odaiba can be an attraction in itself, as the views of the Rainbow Bridge and Tokyo\'s harbor and waterfront area from the Yurikamome elevated train and boats are quite spectacular. Furthermore, it is also possible to walk across the Rainbow Bridge.')
      ],
    ),
    Location(
      name: 'Arashiyama Bamboo Grove 2',
      url: 'https://fluttercrashcourse.com/assets/images/arashiyama@3x.jpg',
      facts: <LocationFact>[
        LocationFact(
            title: 'Summary',
            text:
                'While we could go on about the ethereal glow and seemingly endless heights of this bamboo grove on the outskirts of Kyoto, the sight\'s pleasures extend beyond the visual realm'),
        LocationFact(
            title: 'How to Get There',
            text:
                'Kyoto airport, with several terminals, is located 16 kilometres south of the city and is also known as Kyoto. Kyoto can also be reached by transport links from other regional airports.')
      ],
    ),
    Location(
      name: 'Mount Fuji 2',
      url:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Mount_Fuji_Japan_with_Snow%2C_Lakes_and_Surrounding_Mountains.jpg/800px-Mount_Fuji_Japan_with_Snow%2C_Lakes_and_Surrounding_Mountains.jpg',
      facts: <LocationFact>[
        LocationFact(
            title: 'Summary',
            text:
                'Mount Fuji, located on Honshū, is the highest mountain in Japan at 3,776.24 m (12,389 ft), 2nd-highest peak of an island (volcanic) in Asia, and 7th-highest peak of an island in the world. It is a dormant stratovolcano that last erupted in 1707–1708.'),
        LocationFact(
            title: 'How to Get There',
            text:
                'The closest airport with scheduled international service is Mt. Fuji Shizuoka Airport. It opened in June 2009. It is about 80 kilometres (50 mi) from Mount Fuji. The major international airports serving Tokyo, Tokyo International Airport (Haneda Airport) in Tokyo and Narita International Airport in Chiba, are hours from Mount Fuji.')
      ],
    ),
    Location(
      name: 'Odaiba 2',
      url: 'https://www.japan-guide.com/g18/740/3008_01.jpg',
      facts: <LocationFact>[
        LocationFact(
            title: 'Summary',
            text:
                'Odaiba (お台場) is a popular shopping and entertainment district on a man made island in Tokyo Bay. It originated as a set of small man made fort islands (daiba literally means "fort"), which were built towards the end of the Edo Period (1603-1868) to protect Tokyo against possible attacks from the sea and specifically in response to the gunboat diplomacy of Commodore Perry.'),
        LocationFact(
            title: 'How to Get There',
            text:
                'Access to Odaiba can be an attraction in itself, as the views of the Rainbow Bridge and Tokyo\'s harbor and waterfront area from the Yurikamome elevated train and boats are quite spectacular. Furthermore, it is also possible to walk across the Rainbow Bridge.')
      ],
    ),
    Location(
      name: 'Arashiyama Bamboo Grove 3',
      url: 'https://fluttercrashcourse.com/assets/images/arashiyama@3x.jpg',
      facts: <LocationFact>[
        LocationFact(
            title: 'Summary',
            text:
                'While we could go on about the ethereal glow and seemingly endless heights of this bamboo grove on the outskirts of Kyoto, the sight\'s pleasures extend beyond the visual realm'),
        LocationFact(
            title: 'How to Get There',
            text:
                'Kyoto airport, with several terminals, is located 16 kilometres south of the city and is also known as Kyoto. Kyoto can also be reached by transport links from other regional airports.')
      ],
    ),
    Location(
      name: 'Mount Fuji 3',
      url:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f7/Mount_Fuji_Japan_with_Snow%2C_Lakes_and_Surrounding_Mountains.jpg/800px-Mount_Fuji_Japan_with_Snow%2C_Lakes_and_Surrounding_Mountains.jpg',
      facts: <LocationFact>[
        LocationFact(
            title: 'Summary',
            text:
                'Mount Fuji, located on Honshū, is the highest mountain in Japan at 3,776.24 m (12,389 ft), 2nd-highest peak of an island (volcanic) in Asia, and 7th-highest peak of an island in the world. It is a dormant stratovolcano that last erupted in 1707–1708.'),
        LocationFact(
            title: 'How to Get There',
            text:
                'The closest airport with scheduled international service is Mt. Fuji Shizuoka Airport. It opened in June 2009. It is about 80 kilometres (50 mi) from Mount Fuji. The major international airports serving Tokyo, Tokyo International Airport (Haneda Airport) in Tokyo and Narita International Airport in Chiba, are hours from Mount Fuji.')
      ],
    ),
    Location(
      name: 'Odaiba 3',
      url: 'https://www.japan-guide.com/g18/740/3008_01.jpg',
      facts: <LocationFact>[
        LocationFact(
            title: 'Summary',
            text:
                'Odaiba (お台場) is a popular shopping and entertainment district on a man made island in Tokyo Bay. It originated as a set of small man made fort islands (daiba literally means "fort"), which were built towards the end of the Edo Period (1603-1868) to protect Tokyo against possible attacks from the sea and specifically in response to the gunboat diplomacy of Commodore Perry.'),
        LocationFact(
            title: 'How to Get There',
            text:
                'Access to Odaiba can be an attraction in itself, as the views of the Rainbow Bridge and Tokyo\'s harbor and waterfront area from the Yurikamome elevated train and boats are quite spectacular. Furthermore, it is also possible to walk across the Rainbow Bridge.')
      ],
    )
  ];

  static Location fetch(int index) {
    return MockLocation.items[index];
  }

  static List<Location> fetchAll() {
    return MockLocation.items;
  }

  static Location fetchAny() {
    return MockLocation.items[0];
  }
}
