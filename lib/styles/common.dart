import 'package:flutter/material.dart';

class CommonStyles {
  static Color _hexToColor(String code) {
    return Color(int.parse(code.substring(0, 6), radix: 16) + 0xFF000000);
  }

  static final Color textColorStrong = _hexToColor('000000');

  static final Color textColorDefault = _hexToColor('666666');

  static final Color textColorFaint = _hexToColor('999999');

  static final Color textColorBright = _hexToColor('FFFFFF');

  static final Color accentColor = _hexToColor('FF0000');

  static final String fontNameDefault = 'Montserrat';

  static final textSizeLarge = 16.0;

  static final textSizeDefault = 13.0;

  static const textSizeSmall = 10.0;

  static final textDefault = TextStyle(
    fontFamily: fontNameDefault,
    fontSize: textSizeDefault,
    color: textColorDefault,
  );

  static final navBarTitle = TextStyle(
    fontFamily: fontNameDefault,
    fontWeight: FontWeight.w600,
    fontSize: textSizeDefault,
    color: textColorDefault,
  );
}
