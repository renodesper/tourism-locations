import 'package:tourism_n_co/styles/common.dart';
import 'package:flutter/material.dart';

class LocationTileStyles {
  static final height = 75.0;

  static final titleLight = TextStyle(
    fontFamily: CommonStyles.fontNameDefault,
    fontSize: CommonStyles.textSizeLarge,
    color: CommonStyles.textColorStrong,
  );

  static final titleDark = TextStyle(
    fontFamily: CommonStyles.fontNameDefault,
    fontSize: CommonStyles.textSizeLarge,
    color: CommonStyles.textColorBright,
  );

  static final subtitle = TextStyle(
    fontFamily: CommonStyles.fontNameDefault,
    fontSize: CommonStyles.textSizeDefault,
    color: CommonStyles.textColorFaint,
  );

  static final caption = TextStyle(
    fontFamily: CommonStyles.fontNameDefault,
    fontSize: CommonStyles.textSizeSmall,
    color: CommonStyles.textColorFaint,
  );
}
