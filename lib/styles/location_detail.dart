import 'package:tourism_n_co/styles/common.dart';
import 'package:flutter/material.dart';

class LocationDetailStyles {
  static final headerHorizontalPadding = 20.0;

  static final headerVerticalPadding = 10.0;

  static final bannerImageHeight = 200.0;

  static final factHorizontalPadding = 20.0;

  static final factVerticalPadding = 10.0;

  static final footerHeight = 75.0;

  static final headerLarge = TextStyle(
    fontFamily: CommonStyles.fontNameDefault,
    fontSize: CommonStyles.textSizeLarge,
    color: CommonStyles.textColorStrong,
  );

  static final textCTAButton = TextStyle(
    fontFamily: CommonStyles.fontNameDefault,
    fontSize: CommonStyles.textSizeLarge,
    color: CommonStyles.textColorBright,
  );
}
