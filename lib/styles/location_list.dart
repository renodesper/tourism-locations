class LocationListStyles {
  static final itemHeight = 245.0;

  static final overlayHorizontalPadding = 12.0;

  static final overlayVerticalPadding = 5.0;
}
